<?php

$string['programmingjudgestatus'] = 'Judge状态';
$string['total'] = '提交总数: {$a}';
$string['accepted'] = 'AC数: {$a}';
$string['waiting'] = '等待处理: {$a}';
$string['no.'] = 'No.';
$string['who'] = '姓名';
$string['which'] = '题目';
$string['result'] = '结果';
$string['timeused'] = '用时';
$string['memused'] = '用内存';
$string['submittime'] = '提交时间';
$string['perpageonfulllist'] = '在详细页面上每页列出多少条记录：';

?>
