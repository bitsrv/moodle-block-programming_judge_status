<?php

$string['programmingjudgestatus'] = 'Programming Judge Status';
$string['total'] = 'Total: {$a}';
$string['accepted'] = 'Accepted: {$a}';
$string['waiting'] = 'Waiting: {$a}';
$string['no.'] = 'No.';
$string['who'] = 'Who';
$string['which'] = 'Which';
$string['result'] = 'Result';
$string['timeused'] = 'Time Used';
$string['memused'] = 'Mem Used';
$string['submittime'] = 'Submit Time';
$string['perpageonfulllist'] = 'How many records shows perpage on full list?';

?>
