<?php

include_once($CFG->dirroot.'/mod/programming/lib.php');
include_once($CFG->dirroot.'/lib/tablelib.php');

class block_programming_judge_status extends block_base {

    function init() {
        $this->title = get_string('programmingjudgestatus', 'block_programming_judge_status');
        $this->version = 2007060201;

        $this->config->perpageonfulllist = 20;
    }

    function get_content() {
        global $CFG;

        if ($this->content != NULL) {
            return $this->content;
        }

        if (!isset($this->instance)) {
            return '';
        }

        $courseid = $this->instance->pageid;
        if ($courseid == 1) {
            $total = count_records('programming_submits');
            $passed = count_records('programming_submits', 'passed', 1);
        } else {
            $sql = "SELECT SUM(submitcount) AS total
                      FROM {$CFG->prefix}programming_result AS pr,
                           {$CFG->prefix}programming AS p
                     WHERE p.course = {$courseid}
                       AND p.id = pr.programmingid";
            $total = count_records_sql($sql);
            $sql = "SELECT COUNT(*) AS total
                      FROM {$CFG->prefix}programming_submits AS ps,
                           {$CFG->prefix}programming AS p
                     WHERE p.course = {$courseid}
                       AND p.id = ps.programmingid
                       AND ps.passed = 1";
            $passed = count_records_sql($sql);
        }
        $sql = "SELECT COUNT(*) AS total
                  FROM {$CFG->prefix}programming_testers";
        if ($courseid != 1) {
            $sql .= " AND p.course = $courseid";
        }
        $waiting = count_records_sql($sql);

        $c = '<ul>';
        $c .= '<li>'.get_string('total', 'block_programming_judge_status', $total).'</li>';
        $c .= '<li>'.get_string('accepted', 'block_programming_judge_status', $passed).'</li>';
        $c .= '<li>'.get_string('waiting', 'block_programming_judge_status', $waiting).'</li>';
        $c .= '</ul>';

        $this->content = new stdClass;
        $this->content->text = $c;
        $this->content->footer = '<a href="'.$CFG->wwwroot.'/blocks/programming_judge_status/fulllist.php?id='.$this->instance->id.'">'.get_string('more').'</a>';

        return $this->content;
    }

    function html_attribute() {
        return array('class' => 'sideblock block_'.$this->name);
    }

    function instance_allow_config() {
        return true;
    }
    
}

?>
